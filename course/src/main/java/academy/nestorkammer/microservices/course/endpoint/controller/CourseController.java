package academy.nestorkammer.microservices.course.endpoint.controller;

import academy.nestorkammer.microservices.core.annotation.LogTime;
import academy.nestorkammer.microservices.core.aspect.tracing.TracingMonitor;
import academy.nestorkammer.microservices.core.model.Aluno;
import academy.nestorkammer.microservices.core.model.Course;
import academy.nestorkammer.microservices.course.endpoint.service.CourseService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RequestMapping("v1/admin")
@RestController
@Slf4j
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
@Api(value = "Endpoints to manage course")
public class CourseController {
    private final CourseService courseService;

    @Autowired
    private TracingMonitor tracingMonitor;

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "List all available courses", response = Course[].class)
    @RequestMapping("/cursos")
    @LogTime
    public ResponseEntity<Iterable<Course>> list(Pageable pageable) {
        return new ResponseEntity<>(courseService.list(pageable), HttpStatus.OK);
    }

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Find Curso by Id", response = Course.class)
    @RequestMapping(path="/curso/{id}")
    @LogTime
    public ResponseEntity<Optional<Course>> findById(@PathVariable("id") Long id) {
        return new ResponseEntity<>(courseService.findById(id), HttpStatus.OK);
    }

    @PostMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Create or Save Curso", response = Course.class)
    @RequestMapping("/createOrSaveCurso")
    @LogTime
    public ResponseEntity<Course> createOrSaveCurso(@RequestBody Course newCourse) {
        try{
            return new ResponseEntity<>(courseService.createOrSaveCurso(newCourse), HttpStatus.OK);
        } catch (Exception e) {
            log.error(e.getMessage());
        } finally {
            tracingMonitor.printTrace();
        }
        return new ResponseEntity<>(new Course(), HttpStatus.BAD_REQUEST);
    }

    @PostMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Update Curso", response = Course.class)
    @RequestMapping("/updateCurso")
    @LogTime
    public ResponseEntity<Course> updateCourse(@RequestBody Course newCourse) {
        try{
            return new ResponseEntity<>(courseService.updateCurso(newCourse), HttpStatus.OK);
        } catch (Exception e) {
            log.error(e.getMessage());
        } finally {
            tracingMonitor.printTrace();
        }
        return new ResponseEntity<>(new Course(), HttpStatus.BAD_REQUEST);
    }
    @DeleteMapping("deleteCursoById/{id}")
    @ApiOperation(value = "delete Course By Id", response = Course.class)
    public void deleteCourseById(@PathVariable Long id) {
        courseService.deleteById(id);
    }
}
