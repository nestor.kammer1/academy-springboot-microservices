package academy.nestorkammer.microservices.core.aspect.tracing;

import java.util.List;

public class Method {

    private String methodName;
    private String input;
    private List<Method> methodList;
    private String output;
    private Long timeInMs;

    public Method(String methodName, String input, List<Method> methodList, String output, Long timeInMs) {
        this.methodName = methodName;
        this.input = input;
        this.methodList = methodList;
        this.output = output;
        this.timeInMs = timeInMs;
    }

    public Method(){
    }

    public String getMethodName() {
        return methodName;
    }

    public void setMethodName(String methodName) {
        this.methodName = methodName;
    }

    public String getInput() {
        return input;
    }

    public void setInput(String input) {
        this.input = input;
    }

    public List<Method> getMethodList() {
        return methodList;
    }

    public void setMethodList(List<Method> methodList) {
        this.methodList = methodList;
    }

    public String getOutput() {
        return output;
    }

    public void setOutput(String output) {
        this.output = output;
    }

    public Long getTimeInMs() {
        return timeInMs;
    }

    public void setTimeInMs(Long timeInMs) {
        this.timeInMs = timeInMs;
    }
}