package academy.nestorkammer.microservices.core.aspect.tracing;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

@Aspect
@Service
@Configuration
public class TracingMonitor {
    private static final Logger log = LoggerFactory.getLogger(TracingMonitor.class);

    /*@Around("@annotation(com.nestorkammer.logging.loggingframework.annotation.MethodTracing)")
    public Object checkTracing(ProceedingJoinPoint pj) throws Throwable {

        pushStack((JoinPoint)pj);

        Object oj=pj.proceed();

        popStack((JoinPoint)pj);

        return oj;
    }*/
    
    @Pointcut(value = "execution(* academy.nestorkammer.microservices.aluno.endpoint.service.*Service.*(..))")
    private void executionInService() {
        //do nothing, just for pointcut def
    }

//    @Pointcut(value = "execution(* com.nestorkammer.logging.loggingframework.*Controller.*(..))")
//    private void executionInController() {
//        //do nothing, just for pointcut def
//    }

    @Before(value = "executionInService()")
    public void pushStackInBean(JoinPoint joinPoint) {
        pushStack(joinPoint);
    }

    @AfterReturning(value = "executionInService()", returning = "returnValue")
    public void popStackInBean(Object returnValue) {
        popStack(returnValue);
    }

    ObjectMapper mapper = new ObjectMapper();

    private void pushStack(JoinPoint joinPoint) {
        Method m = new Method();
        m.setMethodName(StringUtils.replace(joinPoint.getSignature().toString(), "com.nestorkammer.logging.loggingframework.service.", ""));
        String input = getInputParametersString(joinPoint.getArgs());
        m.setInput(input);
        m.setTimeInMs(Long.valueOf(System.currentTimeMillis()));
        LoggerThreadLocal.getMethodStack().push(m);
    }

    private String getInputParametersString(Object[] joinPointArgs) {
        String input;
        try {
            input = mapper.writeValueAsString(joinPointArgs);
        } catch (Exception e) {
            input = "Unable to create input parameters string. Error:" + e.getMessage();
        }
        return input;
    }


    private void popStack(Object output) {
        Method childMethod = LoggerThreadLocal.getMethodStack().pop();
        try {
            childMethod.setOutput(output==null?"": mapper.writeValueAsString(output));
        } catch (JsonProcessingException e) {
            childMethod.setOutput(e.getMessage());
        }
        childMethod.setTimeInMs(Long.valueOf(System.currentTimeMillis() - childMethod.getTimeInMs().longValue()));
        if (LoggerThreadLocal.getMethodStack().isEmpty()) {
            LoggerThreadLocal.setMainMethod(childMethod);
        } else {
            Method parentMethod = LoggerThreadLocal.getMethodStack().peek();
            addChildMethod(childMethod, parentMethod);
        }
    }

    private void addChildMethod(Method childMethod, Method parentMethod) {
        if (parentMethod != null) {
            if (parentMethod.getMethodList() == null) {
                parentMethod.setMethodList(new ArrayList<>());
            }
            parentMethod.getMethodList().add(childMethod);
        }
    }

    public void printTrace() {
        try {
            StringBuilder sb = new StringBuilder();
            sb.append("\n<TRACE>\n").append(mapper.writerWithDefaultPrettyPrinter().writeValueAsString(LoggerThreadLocal.getMainMethod()));
            sb.append("\n</TRACE>");
            //System.out.println(sb.toString());
            log.debug(sb.toString());
        } catch (JsonProcessingException e) {
            StringUtils.abbreviate(ExceptionUtils.getStackTrace(e), 2000);
        }
    }
}
