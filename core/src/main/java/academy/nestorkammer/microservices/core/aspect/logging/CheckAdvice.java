package academy.nestorkammer.microservices.core.aspect.logging;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.Random;

@Aspect
@Component
public class CheckAdvice {
    private static final Logger log = LoggerFactory.getLogger(CheckAdvice.class);

    @Around("@annotation(academy.nestorkammer.microservices.core.annotation.LogTime)")
    public Object checkTime(ProceedingJoinPoint pj) throws Throwable {
        LocalDateTime start = LocalDateTime.now();
        HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes()).getRequest();

        long memoriaLivre1 = Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory();
        memoriaLivre1 = ((memoriaLivre1/1024)/1024); //para MB
        String[] modulos = new String[5];
        modulos[0] = "Admin";
        modulos[1] = "Cadastros";
        modulos[2] = "Operacoes";
        modulos[3] = "Alunos";
        modulos[4] = "Docentes";

        Random gerador = new Random();
        long numDado =  (gerador.nextInt(4)+1);

        Object oj=pj.proceed();
        LocalDateTime end = LocalDateTime.now();
        long milliseconds = start.until(end, ChronoUnit.MILLIS);

        ObjectMapper om = new ObjectMapper();
        String statusCode = "";
        String statusCodeValue = "";

        try {
            JsonNode array = om.readValue(om.writeValueAsString(oj), JsonNode.class);
            statusCode = array.get("statusCode").toString();
            statusCodeValue = array.get("statusCodeValue").toString();

        }catch (Exception e){
            statusCode = e.getMessage();
            statusCodeValue = "-1";
        }

        StringBuffer jsonText = new StringBuffer();
        jsonText.append("{\"sistema\": \"" ).append("x-tema")
                .append("\", \"modulo\": \"").append( modulos[(int)numDado])
                .append("\", \"classe\": \"").append( pj.getSignature())
                .append("\", \"metodo\": \"").append( pj.getSignature().getName())
                .append("\", \"startsAt\": \"" ).append( start)
                .append("\", \"endsAt\": \"").append(end)
                .append("\", \"totalTime\": ").append(((end.getSecond()-start.getSecond())*1000))
                .append(", \"memoriaLivreMb\": ").append(memoriaLivre1)
                .append(", \"statusCode\": " ).append(statusCode)
                .append(", \"statusCodeValue\": ").append(statusCodeValue)
                .append(", \"serverName\": \"" ).append(request.getServerName())
                .append("\", \"serverPort\": ").append(request.getServerPort())
                .append(", \"method\": \"" ).append(request.getMethod())
                .append("\", \"requestURL\": \"" ).append(request.getRequestURL().toString())
                .append("\"}");

            log.warn(jsonText.toString());

        return oj;

    }

    @Around("@annotation(academy.nestorkammer.microservices.core.annotation.LogRequest)")
    public Object checkRequest(ProceedingJoinPoint pj) throws Throwable {
        ObjectMapper om = new ObjectMapper();
        //log.debug("In method: " + pj.getSignature() + " - With request as : " + om.writeValueAsString(pj.getArgs()));
        return pj.proceed();
    }

    @Around("@annotation(academy.nestorkammer.microservices.core.annotation.LogResponse)")
    public Object checkResponse(ProceedingJoinPoint pj) throws Throwable {
        ObjectMapper om = new ObjectMapper();
        Object oj=pj.proceed();
        //log.debug("In method: " + pj.getSignature() + " - With response as : " + om.writeValueAsString(oj));
        return oj;
    }

}
