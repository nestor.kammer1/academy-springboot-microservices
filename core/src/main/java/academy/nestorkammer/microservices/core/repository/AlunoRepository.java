package academy.nestorkammer.microservices.core.repository;

import academy.nestorkammer.microservices.core.model.Aluno;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface AlunoRepository extends PagingAndSortingRepository<Aluno, Long> {
}
