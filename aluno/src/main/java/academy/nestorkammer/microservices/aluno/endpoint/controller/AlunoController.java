package academy.nestorkammer.microservices.aluno.endpoint.controller;

import academy.nestorkammer.microservices.core.annotation.LogTime;
import academy.nestorkammer.microservices.core.aspect.tracing.TracingMonitor;
import academy.nestorkammer.microservices.core.model.Aluno;
import academy.nestorkammer.microservices.aluno.endpoint.service.AlunoService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.Null;
import java.util.Iterator;
import java.util.Optional;

@RequestMapping("v1/admin")
@RestController
@Slf4j
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
@Api(value = "Endpoints to manage aluno")
public class AlunoController {
    private final AlunoService alunoService;

    @Autowired
    private TracingMonitor tracingMonitor;

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "List all available alunos", response = Aluno[].class)
    @RequestMapping("/aluno")
    @LogTime
    public ResponseEntity<Iterable<Aluno>> list(Pageable pageable) {
        try {
            return new ResponseEntity<>(alunoService.list(pageable), HttpStatus.OK);
        } catch (Exception e) {
            log.error(e.getMessage());
        } finally {
            tracingMonitor.printTrace();
        }
        return new ResponseEntity<>(new Iterable<Aluno>() {
            @Override
            public Iterator<Aluno> iterator() {
                return null;
            }
        }, HttpStatus.BAD_REQUEST);
    }

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Find aluno by Id", response = Aluno.class)
    @RequestMapping(path="/aluno/{id}")
    @LogTime
    public ResponseEntity<Optional<Aluno>> findById(@PathVariable("id") Long id) {
        return new ResponseEntity<>(alunoService.findById(id), HttpStatus.OK);
    }

    @PostMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Create or Save Aluno", response = Aluno.class)
    @RequestMapping("/createOrSaveAluno")
    @LogTime
    public ResponseEntity<Aluno> createOrSaveAluno(@RequestBody Aluno newAluno) {
        try{
            return new ResponseEntity<>(alunoService.createOrSaveAluno(newAluno), HttpStatus.OK);
        } catch (Exception e) {
            log.error(e.getMessage());
        } finally {
            tracingMonitor.printTrace();
        }
        return new ResponseEntity<>(new Aluno(), HttpStatus.BAD_REQUEST);
    }

    @PostMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Update Aluno", response = Aluno.class)
    @RequestMapping("/updateAluno")
    @LogTime
    public ResponseEntity<Aluno> updateAluno(@RequestBody Aluno newAluno) {
        try{
            return new ResponseEntity<>(alunoService.updateAluno(newAluno), HttpStatus.OK);
        } catch (Exception e) {
            log.error(e.getMessage());
        } finally {
            tracingMonitor.printTrace();
        }
        return new ResponseEntity<>(new Aluno(), HttpStatus.BAD_REQUEST);
    }
    @DeleteMapping("deleteAlunoById/{id}")
    @ApiOperation(value = "delete Aluno By Id", response = Aluno.class)
    public void deleteAlunoById(@PathVariable Long id) {
        alunoService.deleteById(id);
    }
}
