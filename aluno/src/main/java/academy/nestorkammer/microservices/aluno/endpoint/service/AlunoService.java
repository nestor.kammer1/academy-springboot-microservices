package academy.nestorkammer.microservices.aluno.endpoint.service;

import academy.nestorkammer.microservices.core.annotation.LogTime;
import academy.nestorkammer.microservices.core.model.Aluno;
import academy.nestorkammer.microservices.core.repository.AlunoRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.Optional;

@Service
@Slf4j
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class AlunoService {
    private final AlunoRepository alunoRepository;
    public Iterable<Aluno> list(Pageable pageable) {
        log.info("Listing all alunos.");
        try {
            log.warn(this.fazerAlgumaCoisa("listAll"));
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return alunoRepository.findAll(pageable);
    }

    public String fazerAlgumaCoisa(String aaa) throws InterruptedException {
        Thread.sleep(2000);

        log.error("Ocorreu um erro qualquer A.");
        log.info("Ocorreu um erro qualquer B.");
        log.warn("Ocorreu um erro qualquer C.");

        return "Feito alguma coisa : " + aaa;
    }

    public Optional<Aluno> findById(Long id) {
        try {
            log.warn(this.fazerAlgumaCoisa("find by id"));
        } catch (Exception e){
            log.error(e.getMessage());
        }
        return alunoRepository.findById(id);
    }

    public Aluno createOrSaveAluno(Aluno newAluno) {
        return alunoRepository.save(newAluno);
    }

    public Aluno updateAluno(Aluno newAluno) {
        return alunoRepository.findById(newAluno.getId()).map(aluno -> {
            aluno.setNome(newAluno.getNome());
            return alunoRepository.save(aluno);
        }).orElseGet(() -> {
            return alunoRepository.save(newAluno);
        });
    }

    public void deleteById(Long id) {
        alunoRepository.deleteById(id);
    }
}
