package academy.nestorkammer.microservices.auth.endpoint.controller;

import academy.nestorkammer.microservices.core.annotation.LogTime;
import academy.nestorkammer.microservices.core.aspect.logging.CheckAdvice;
import academy.nestorkammer.microservices.core.aspect.tracing.TracingMonitor;
import academy.nestorkammer.microservices.core.model.ApplicationUser;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.security.Principal;

@RestController
@RequestMapping("user")
@Api(value = "Endpoints to manage user´s information")
@Slf4j
public class UserInfoController {
    //private static final Logger log = LoggerFactory.getLogger(UserInfoController.class);

    @Autowired
    private TracingMonitor tracingMonitor;

    @GetMapping(path="info", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ApiOperation(value = "Will retrieve then information from the user available into the token", response = ApplicationUser.class)
    @LogTime
    public ResponseEntity<ApplicationUser> getUserInfo(Principal principal) {
        try {
            ApplicationUser applicationUser = (ApplicationUser) ((UsernamePasswordAuthenticationToken) principal).getPrincipal();
            return new ResponseEntity<>(applicationUser, HttpStatus.OK);
        }
        catch (Exception e){
            log.error(e.getMessage());
        }
        finally {
            tracingMonitor.printTrace();
        }
        return new ResponseEntity<>( new ApplicationUser(), HttpStatus.BAD_REQUEST);
    }
}
